# SailfishOS for Xiaomi MI6 (SAGIT)

This repository uses gitlab-ci to build the sailfish images for the Xiaomi MI6 (sagit)


# Files

[sagit.env](https://gitlab.com/Verevka/sagit-ci/blob/main/sagit.env) contains all the environment variables required for the process. On every release, version number is changed in the master branch and a new tag is created with the release number to trigger the build process.

[Jolla-@RELEASE@-sagit-@ARCH@.ks](https://gitlab.com/Verevka/sagit-ci/blob/main/Jolla-@RELEASE@-sagit-@ARCH@.ks) file is the kickstart file which is used by PlatformSDK image, this files contains device specific repositories, Repositories can be changed by from devel to testing or vice versa.

[run-mic.sh](https://gitlab.com/Verevka/sagit-ci/blob/main/run-mic.sh) is a simple bash script, which executes the build.

# Install

1. Install LineageOS-17.1
2. Disable data encription
3. Install SailfishOS.zip

# Download

[Download the latest build](https://gitlab.com/Verevka/sagit-ci/-/jobs?scope=finished)

# Source code
[https://github.com/SailfishOS-sagit](https://github.com/SailfishOS-sagit)
